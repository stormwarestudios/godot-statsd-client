# Godot StatsD Client

## Introduction

Godot StatsD Client (GSC) is part of an initiative to improve Godot's presence in the cloud.

## Architecture

```mermaid
sequenceDiagram
  autonumber
  
  alt Main Thread
    Game ->>+ StatsD Client: initialize
    StatsD Client ->>- StatsD Client: start thread
  
    Game ->>+ StatsD Client: report metrics
    StatsD Client ->>- Message Queue: enqueue metrics data
  end

  loop Process Messages Thread
    Message Queue ->>+ Messages Thread: dequeue messages
    Messages Thread ->> StatsD Server: publish UDP frames
    Messages Thread ->>- Messages Thread: sleep
  end
```

## Quick Setup

* Clone this repository.
* Add StatsDClient as an [Autoloading Singleton](https://docs.godotengine.org/en/stable/tutorials/scripting/singletons_autoload.html) in your Godot project.
* Somewhere in your code, initialize the StatsDClient with the following:
  ```gdscript
  var options = { ... }
  StatsDClient.initialize(options)
  ```
* Your game server will now attempt connectionless UDP packet transmission to the host specified in the `initialize()` call, each time you invoke a metrics function.

## Initialization

Godot StatsD Client can be initialized with a dictionary containing a combination of the following:

| Option                 | Description                                                                                                                                                                  |
|------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `host`                 | The StatsD host to send metrics data. `default: 127.0.0.1`                                                                                                                   |
| `port`                 | The UDP port the StatsD server is listening on. `default: 8125`                                                                                                              |
| `prefix`               | Optional text prepended to all metric names. `default: ""`                                                                                                                   |
| `suffix`               | Optional text appended to al metric names. `default: ""`                                                                                                                     |
| `logger` (coming soon) | A [FuncRef](https://docs.godotengine.org/en/stable/classes/class_funcref.html) with the format `func (arg1:String)`, called when logging text. Uses `print_debug` otherwise. |
| `global_tags`          | A set of tags applied to every metric.                                                                                                                                       |

## Usage

All StatsD functions follow the same underlying pattern:

| Variable | Meaning                                                                             |
|----------|-------------------------------------------------------------------------------------|
| name     | A name of your choosing for a particular stat. `required`                           |
| value    | The stat value. `required except in increment/decrement`.                           |
| tags     | A Dictionary containing key-value pairs of tags to be added to metrics. `optional`  |

## Testing

To preview the messages being sent by the Godot StatsD Client, you can use the tool `netcat` to listen on the UDP port and echo the messages received.

```bash
nc -u -l -p 8125
```

### API

#### Main StatsD Functions

```gdscript
## Initialize the StatsD client. Returns 0 or an Error (see https://docs.godotengine.org/en/stable/classes/class_@globalscope.html#enum-globalscope-error).
initialize(options:Dictionary = {})

# Counter: increment a counter by 1.
increment(name:String, tags:Dictionary = {}, rate:float = 1.0) -> void

# Counter: decrement a counter by 1.
decrement(name:String, tags:Dictionary = {}, rate:float = 1.0) -> void

# Counter: modify a counter by a given value (positive or negative).
counter(name:String, value:int, tags:Dictionary = {}, rate:float = 1.0) -> void

# Counter: alias for counter().
sample(name:String, value:int, tags:Dictionary = {}, rate:float = 1.0) -> void

# Gauge: set a gauge to a specified value.
gauge(name:String, value:int, tags:Dictionary = {}, rate:float = 1.0) -> void

# Timing: record the time elapsed in milliseconds.
timing(name:String, value:int, tags:Dictionary = {}, rate:float = 1.0) -> void

# Timing: alias for timing().
histogram(name:String, value:int, tags:Dictionary = {}, rate:float = 1.0) -> void

# Set: count the unique occurrences of a stat.
setting(name:String, value, tags:Dictionary = {}, rate:float = 1.0) -> void
```

#### Utility Functions

```gdscript
# Set the global tags sent with every stat.
set_global_tags(tags:Dictionary) -> void

# Convert a dictionary to a StatsD-compatible tag string.
dictionary_to_tag_string(tags:Dictionary) -> String
```

## References

* [StatsD Metric Types](https://github.com/statsd/statsd/blob/master/docs/metric_types.md)
