# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2022-05-13

### Added

- New `initialize()` option, `mtu`.
- Build UDP frames up to `mtu` bytes before sending.

## [0.2.0] - 2022-05-12

### Changed

- Moved metrics packet sending to its own thread.

## [0.1.0] - 2022-05-08

### Added

- StatsD UDP client.
- Metrics reporting for counting, sampling, timing, gauges and sets.
- Convenience monitor-info function.
