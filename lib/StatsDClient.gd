extends Node
class_name StatsDClient


const STATSD_HOST = "127.0.0.1"
const STATSD_PORT = 8125
const STATSD_BUFFER_SIZE = 1432

var socket:PacketPeerUDP
var name_prefix:String = ""
var name_suffix:String = ""
var global_tags:String = ""
var buffer_size:int = STATSD_BUFFER_SIZE
var logger = null
var message_queue = []
var message_processing_thread:Thread
var message_processing_mutex:Mutex
var message_processing_enabled:bool = true
var message_processing_thread_delay_msec:int = 1000


func initialize(options:Dictionary = {}):
	var host    = _env_dict_default("STATSD_HOST",        options, "host",   STATSD_HOST)
	var port    = _env_dict_default("STATSD_PORT",        options, "port",   STATSD_PORT)
	name_prefix = _env_dict_default("STATSD_PREFIX",      options, "prefix", "")
	name_suffix = _env_dict_default("STATSD_SUFFIX",      options, "suffix", "")
	buffer_size = _env_dict_default("STATSD_BUFFER_SIZE", options, "mtu", STATSD_BUFFER_SIZE)
	logger      = _env_dict_default("",                   options, "logger", self)
	var tags    = _env_dict_default("",                   options, "global_tags", {})
	set_global_tags(tags)
	
	socket = PacketPeerUDP.new()
	
	# Create a processing thread
	message_processing_thread = Thread.new()
	message_processing_mutex = Mutex.new()
	var thread_started = message_processing_thread.start(self, "_process_message_queue")
	if thread_started != OK:
		logger.error("Failed to create thread: %s" % [ERR_CANT_CONNECT])
		return null
	
	return _start_client(host, int(port))


func debug(text:String) -> void:
	print_debug(text)


func error(text:String) -> void:
	print_debug(text)
	print_stack()


func _process_message_queue():
	while message_processing_enabled:
		message_processing_mutex.lock()
		_process_messages()
		message_processing_mutex.unlock()
		OS.delay_msec(message_processing_thread_delay_msec)


func _env_dict_default(env:String, dict:Dictionary, key:String, default = null):
	var retval
	if OS.has_environment(env):
		var setting = OS.get_environment(env)
		if setting != null:
			retval = setting
	elif dict.has(key):
		retval = dict[key]
	else:
		retval = default
	
	if typeof(default) == TYPE_INT:
		return int(retval)
	elif typeof(default) == TYPE_REAL:
		return float(retval)
	else:
		return retval


func _process_messages() -> void:
	var buffer:String
	var count = 0
	while message_queue.size() > 0:
		var message = _compose_packet(message_queue[0])
		if buffer.length() == 0:
			buffer = message + "\n"
		else:
			if buffer.length() + message.length() + 1 <= buffer_size:
				buffer = buffer + message + "\n"
			else:
				#print("Sending buffer (%s bytes, %s messages)" % [ buffer.length(), count ])
				var err = _send(buffer)
				if err != OK:
					return
				count = 0
				buffer = ""
		
		message_queue.pop_front()
		count = count + 1


func _compose_packet(message):
	return "%s#%s:%s|%s|@%s" % [ 
			name_prefix + message.name + name_suffix, 
			global_tags + "," + dictionary_to_tag_string(message.tags), 
			message.value, 
			message.type, 
			message.rate 
		]


func _exit_tree():
	socket.close()
	message_processing_thread.wait_to_finish()


func _start_client(host:String, port:int):
	return socket.connect_to_host(host, port)


func _send(packet:String):
	var err = socket.put_packet(packet.to_ascii())
	if err:
		print("Sending error: %s" % [ err ])
		return err
	return OK

func get_performance_monitor_info():
	return {
		"time_fps": Performance.get_monitor(Performance.TIME_FPS),
		"time_process": Performance.get_monitor(Performance.TIME_PROCESS),
		"time_physics_process": Performance.get_monitor(Performance.TIME_PHYSICS_PROCESS),
		"memory_static": Performance.get_monitor(Performance.MEMORY_STATIC),
		"memory_dynamic": Performance.get_monitor(Performance.MEMORY_DYNAMIC),
		"memory_static_max": Performance.get_monitor(Performance.MEMORY_STATIC_MAX),
		"memory_dynamic_max": Performance.get_monitor(Performance.MEMORY_DYNAMIC_MAX),
		"memory_message_buffer_max": Performance.get_monitor(Performance.MEMORY_MESSAGE_BUFFER_MAX),
		"object_count": Performance.get_monitor(Performance.OBJECT_COUNT),
		"object_resource_count": Performance.get_monitor(Performance.OBJECT_RESOURCE_COUNT),
		"object_node_count": Performance.get_monitor(Performance.OBJECT_NODE_COUNT),
		"object_orphan_node_count": Performance.get_monitor(Performance.OBJECT_ORPHAN_NODE_COUNT),
		"render_objects_in_frame": Performance.get_monitor(Performance.RENDER_OBJECTS_IN_FRAME),
		"render_vertices_in_frame": Performance.get_monitor(Performance.RENDER_VERTICES_IN_FRAME),
		"render_material_changes_in_frame": Performance.get_monitor(Performance.RENDER_MATERIAL_CHANGES_IN_FRAME),
		"render_shader_changes_in_frame": Performance.get_monitor(Performance.RENDER_SHADER_CHANGES_IN_FRAME),
		"render_surface_changes_in_frame": Performance.get_monitor(Performance.RENDER_SURFACE_CHANGES_IN_FRAME),
		"render_draw_calls_in_frame": Performance.get_monitor(Performance.RENDER_DRAW_CALLS_IN_FRAME),
		"render_2d_items_in_frame": Performance.get_monitor(Performance.RENDER_2D_ITEMS_IN_FRAME),
		"render_2d_draw_calls_in_frame": Performance.get_monitor(Performance.RENDER_2D_DRAW_CALLS_IN_FRAME),
		"render_video_mem_used": Performance.get_monitor(Performance.RENDER_VIDEO_MEM_USED),
		"render_texture_mem_used": Performance.get_monitor(Performance.RENDER_TEXTURE_MEM_USED),
		"render_vertex_mem_used": Performance.get_monitor(Performance.RENDER_VERTEX_MEM_USED),
		"render_usage_video_mem_total": Performance.get_monitor(Performance.RENDER_USAGE_VIDEO_MEM_TOTAL),
		"physics_2d_active_objects": Performance.get_monitor(Performance.PHYSICS_2D_ACTIVE_OBJECTS),
		"physics_2d_collision_pairs": Performance.get_monitor(Performance.PHYSICS_2D_COLLISION_PAIRS),
		"physics_2d_island_count": Performance.get_monitor(Performance.PHYSICS_2D_ISLAND_COUNT),
		"physics_3d_active_objects": Performance.get_monitor(Performance.PHYSICS_3D_ACTIVE_OBJECTS),
		"physics_3d_collision_pairs": Performance.get_monitor(Performance.PHYSICS_3D_COLLISION_PAIRS),
		"physics_3d_island_count": Performance.get_monitor(Performance.PHYSICS_3D_ISLAND_COUNT),
		"audio_output_latency": Performance.get_monitor(Performance.AUDIO_OUTPUT_LATENCY),
	}


func set_global_tags(tags:Dictionary) -> void:
	global_tags = dictionary_to_tag_string(tags)
	logger.debug("Using %s" % [global_tags])


func dictionary_to_tag_string(tags:Dictionary) -> String:
	var tagstr = ""
	var keys = tags.keys();
	for key in keys: 
		var val = tags[key]
		if tagstr.length() == 0:
			tagstr = "%s=%s" % [ key, val ]
		else:
			tagstr = "%s,%s=%s" % [ tagstr, key, val ]
	return tagstr


func increment(name:String, tags:Dictionary = {}, rate:float = 1.0) -> void:
	message_processing_mutex.lock()
	message_queue.push_back({
		"name": name,
		"value": 1,
		"rate": rate,
		"type": "c",
		"tags": tags
	})
	message_processing_mutex.unlock()


func decrement(name:String, tags:Dictionary = {}, rate:float = 1.0) -> void:
	message_processing_mutex.lock()
	message_queue.push_back({
		"name": name,
		"value": -1,
		"rate": rate,
		"type": "c",
		"tags": tags
	})
	message_processing_mutex.unlock()


func counter(name:String, value:int, tags:Dictionary = {}, rate:float = 1.0) -> void:
	message_processing_mutex.lock()
	message_queue.push_back({
		"name": name,
		"value": value,
		"rate": rate,
		"type": "c",
		"tags": tags
	})
	message_processing_mutex.unlock()


func gauge(name:String, value:int, tags:Dictionary = {}, rate:float = 1.0) -> void:
	message_processing_mutex.lock()
	message_queue.push_back({
		"name": name,
		"value": value,
		"rate": rate,
		"type": "g",
		"tags": tags
	})
	message_processing_mutex.unlock()


func sample(name:String, value:int, tags:Dictionary = {}, rate:float = 1.0) -> void:
	counter(name, value, tags, rate)


func histogram(name:String, value:int, tags:Dictionary = {}, rate:float = 1.0) -> void:
	timing(name, value, tags, rate)


func setting(name:String, value, tags:Dictionary = {}, rate:float = 1.0) -> void:
	message_processing_mutex.lock()
	message_queue.push_back({
		"name": name,
		"value": value,
		"rate": rate,
		"type": "s",
		"tags": tags
	})
	message_processing_mutex.unlock()


func timing(name:String, value:int, tags:Dictionary = {}, rate:float = 1.0) -> void:
	message_processing_mutex.lock()
	message_queue.push_back({
		"name": name,
		"value": value,
		"rate": rate,
		"type": "ms",
		"tags": tags
	})
	message_processing_mutex.unlock()
