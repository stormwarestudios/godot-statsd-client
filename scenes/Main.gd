extends Spatial


var statsd:StatsDClient

func _ready() -> void:
	statsd = StatsDClient.new()
	add_child(statsd)
	
	statsd.initialize({
		"host": "127.0.0.1",
		"port": 8125,
		"global_tags": {
			"location_id": "1245678-abcd-1234-cdef-1234567890ab",
			"env": "prod",
			"server_type": "location",
			"project": "xmd"
		}
	})


func _on_Timer_timeout() -> void:
#	statsd.increment("performance.counter")
#	statsd.gauge("foo_gauge", rand_range(1, 1000))
	statsd.histogram("performance.fps", Performance.get_monitor(Performance.TIME_FPS))
	statsd.histogram("performance.memory_dynamic", Performance.get_monitor(Performance.MEMORY_DYNAMIC))
	statsd.histogram("performance.memory_static", Performance.get_monitor(Performance.MEMORY_STATIC))
	statsd.histogram("performance.object_count", Performance.get_monitor(Performance.OBJECT_COUNT))
	statsd.histogram("performance.object_node_count", Performance.get_monitor(Performance.OBJECT_NODE_COUNT))
	statsd.histogram("performance.time_physics_process", Performance.get_monitor(Performance.TIME_PHYSICS_PROCESS))
	statsd.histogram("performance.time_process", Performance.get_monitor(Performance.TIME_PROCESS))

